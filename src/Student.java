import java.util.Scanner;

public class Student {

    Scanner input = new Scanner(System.in);


    private int[] kumpulanNilai;
    private String konvert;
    private String nama;
    private int banyakNilai;
    private int sums = 0;
    private int rata;

    Student() {

    }

    Student(String nama, int banyakNilai, int[] kumpulanNilai, int sums, int rata, String konvert){
        setNama(nama);
        setBanyakNilai(banyakNilai);
        setKumpulanNilai(kumpulanNilai);
        setSums(sums);
        setRata(rata);
        setKonvert(konvert);

    }


    void LinkOStart() {
        System.out.print("Masukkan Nama Siswa : ");
        nama = input.nextLine();
        System.out.print("Masukkan Banyak Nilai : ");
        banyakNilai = Integer.parseInt(input.nextLine());
        kumpulanNilai = new int[banyakNilai];
        for (int i = 1; i <= banyakNilai; i++){
            System.out.print("Masukkan Nilai " + i + " : ");
            int nilaiSatuan;
            nilaiSatuan = Integer.parseInt(input.nextLine());
            kumpulanNilai[i-1] = nilaiSatuan;
        }

        total(kumpulanNilai);
        rerata(sums);
        konvert(rata);


    }




    int total(int[] kumpulanNilai){
        for (int i : kumpulanNilai){
            sums += i;
        }
        System.out.println("Total Nilai : " + sums);
        return sums;
    }

    int rerata(int sums){
        rata = sums / banyakNilai;
        System.out.println("Rata rata : " + rata);
       return rata;
    }

    void konvert(int rata) {
        if (rata >= 90) {
            konvert = "A";
            System.out.println("Index Nilai : " + konvert);
        }
        else if (rata >= 70) {
            konvert = "B";
            System.out.println("Index Nilai : " + konvert);;
        }
        else if (rata >= 50) {
            konvert = "C";
            System.out.println("Index Nilai : " + konvert);;
        }
    }

    public String getNama() {
        return nama;
    }

    public int[] getKumpulanNilai() {
        return kumpulanNilai;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setBanyakNilai(int banyakNilai) {
        this.banyakNilai = banyakNilai;
    }

    public int getBanyakNilai() {
        return banyakNilai;
    }

    public void setKumpulanNilai(int[] kumpulanNilai) {
        this.kumpulanNilai = kumpulanNilai;
    }

    public void setSums(int sums) {
        this.sums = sums;
    }

    public int getSums() {
        return sums;
    }

    public void setRata(int rata) {
        this.rata = rata;
    }

    public int getRata() {
        return rata;
    }

    public void setKonvert(String konvert) {
        this.konvert = konvert;
    }

    public String getKonvert() {
        return konvert;
    }
}
